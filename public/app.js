var app = angular.module('myapp', ['ngRoute']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $locationProvider.hashPrefix('!');
    $routeProvider.when('/inicio', {
        templateUrl: 'views/inicio.html',
        controller: 'ctrlInicio'
    }).otherwise({
        redirectTo: '/inicio'
    })
}])