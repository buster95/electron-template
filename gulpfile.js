var gulp = require('gulp');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var minjs = require('gulp-uglify');
var mincss = require('gulp-clean-css');
var plumber = require('gulp-plumber');

var inject = require('gulp-inject');
var bowerFiles = require('main-bower-files');

var dirjs = [
    './public/extends/*.js',
    './public/app.js',
    './public/app_services/*.js',
    './public/app_directives/*.js',
    './public/app_controllers/*.js'
];

var dircss = [
    './public/css/*.less'
];

gulp.task('default', function () {
    gulp.watch(dirjs, ['js_processing']);
});

gulp.task('js_processing', function () {
    gulp.src(dirjs)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        // .pipe(concat('script.min.js'))
        // .pipe(gulp.dest('public/'))
        .pipe(concat('script.min.js'))
        .pipe(minjs())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('public/'));
});

gulp.task('inject', function () {
    var inyectingFiles = gulp.src(['./public/*.min.js'], { read: false });
    var inyectingBower = gulp.src(bowerFiles(), { read: false });
    return gulp.src('./public/index.html')
        .pipe(inject(inyectingBower, { name: 'bower', relative: true }))
        .pipe(inject(inyectingFiles, { relative: true }))
        // .pipe(inject(es.merge(
        //     cssFiles,
        //     gulp.src('./src/app/**/*.js', { read: false })
        // )))
        .pipe(gulp.dest('./public'));
});